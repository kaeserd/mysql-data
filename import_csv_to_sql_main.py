# -*- coding: utf-8 -*-


##############################################################################
# Before running this script, make sure you created a local mysql database
# change the database username and password in the settings below
#
# The script will drop any existing table with the same name and schema then 
# set in the settings, then read all csv files that contain the key word 
# (string) and create a new table appending all data from the fitting csv files
##############################################################################
from sqlalchemy import create_engine
import import_csv_to_sql_functions as sqlf

##############################################################################
# Settings:
# 
# directory of data

data_dir = 
#
# db username
db_username = "root"
# 
db_password = 
# 
# name of mysql schema to add table
mysql_schema = "test"
#
# name of mysql table to add data in
mysql_table = "Test_Actuel_Gen"
#
##############################################################################



if __name__ == '__main__':

    
    engine = create_engine('mysql+pymysql://'+db_username+':'+\
                           db_password+'@localhost/')
    connection = engine.connect()
    sqlf.insert_into_sql_table(engine, data_dir, mysql_schema, mysql_table, 
                               filtering_kw)
    print("Done!")
