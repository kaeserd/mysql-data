-- MySQL dump 10.13 Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost Database: power_plants_raw
-- ------------------------------------------------------
-- Server version 8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plant_information`
--

DROP TABLE IF EXISTS `plant_information`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plant_information` (
`ResolutionCode` text,
`AreaCode` text,
`AreaTypeCode` text,
`AreaName` text NOT NULL,
`MapCode` text,
`GenerationUnitEIC` text,
`PowerSystemResourceName` text NOT NULL,
`ProductionType` text NOT NULL,
`InstalledGenCapacity` double DEFAULT NULL,
`id_plant` int NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id_plant`),
KEY `country` (`MapCode`(500)),
KEY `installed` (`InstalledGenCapacity`),
KEY `production_type` (`ProductionType`(500)),
KEY `time_intervall` (`ResolutionCode`(500)),
KEY `cta` (`AreaTypeCode`(500))
) ENGINE=InnoDB AUTO_INCREMENT=2154 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plant_information`
--
-- WHERE: id_plant < 10

LOCK TABLES `plant_information` WRITE;
/*!40000 ALTER TABLE `plant_information` DISABLE KEYS */;
INSERT INTO `plant_information` VALUES ('PT60M','10YGR-HTSO-----Y','CTA','GR CTA','GR','29WGU-ALOUMINIOF','ALOUMINIO','Fossil Gas',334,1),('PT60M','10YFI-1--------U','CTA','FI CTA','FI','44W-00000000010U','Äänekoski G1','Biomass',260,2),('PT60M','10YRO-TEL------P','CTA','RO CTA','RO','30WMINTMINT6---I','CET_MINT6_CA','Fossil Hard coal',195,3),('PT60M','10YCH-SWISSGRIDZ','CTA','CH CTA','CH','12W-0000000431-4','Beznau 1','Nuclear',365,4),('PT60M','10YPT-REN------W','CTA','PT CTA','PT','16W-ALQUE1-----X','Alqueva - G1','Hydro Pumped Storage',127,5),('PT60M','10YES-REE------0','CTA','ES CTA','ES','18WABO1-12345-D7','ABO¿O 1','Fossil Hard coal',341.7,6),('PT60M','10YRO-TEL------P','CTA','RO CTA','RO','30WTE-PARO124--W','CET_Paroseni_CA','Fossil Hard coal',133,7),('PT60M','10YPT-REN------W','CTA','PT CTA','PT','16W-ALQUE2-----Q','Alqueva - G2','Hydro Pumped Storage',127,8),('PT60M','10YES-REE------0','CTA','ES CTA','ES','18WABO2-12345-DY','ABO¿O 2','Fossil Hard coal',561.8,9);
/*!40000 ALTER TABLE `plant_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-09 21:36:35
