-- MySQL dump 10.13 Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost Database: power_plants_raw
-- ------------------------------------------------------
-- Server version 8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_gen_raw`
--

DROP TABLE IF EXISTS `act_gen_raw`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_gen_raw` (
`DateTime` text,
`ResolutionCode` text,
`AreaCode` text,
`AreaTypeCode` text,
`AreaName` text,
`MapCode` text,
`GenerationUnitEIC` text,
`PowerSystemResourceName` text,
`ProductionType` text,
`ActualGenerationOutput` double DEFAULT NULL,
`ActualConsumption` double DEFAULT NULL,
`InstalledGenCapacity` double DEFAULT NULL,
`UpdateTime` text,
`id_plant` int NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id_plant`)
) ENGINE=InnoDB AUTO_INCREMENT=110527853 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_gen_raw`
--
-- WHERE: id_plant < 10

LOCK TABLES `act_gen_raw` WRITE;
/*!40000 ALTER TABLE `act_gen_raw` DISABLE KEYS */;
INSERT INTO `act_gen_raw` VALUES ('2014-12-10 20:00:00.000','PT60M','10YSE-1--------K','CTA','SE CTA','SE','46WGU0000000046R','Stalon G1','Hydro Water Reservoir',118.9,NULL,120,'2017-09-13 22:40:20',1),('2014-12-10 21:00:00.000','PT60M','10YSE-1--------K','CTA','SE CTA','SE','46WGU0000000046R','Stalon G1','Hydro Water Reservoir',119.5,NULL,120,'2017-09-13 22:40:20',2),('2014-12-10 22:00:00.000','PT60M','10YSE-1--------K','CTA','SE CTA','SE','46WGU0000000046R','Stalon G1','Hydro Water Reservoir',120.5,NULL,120,'2017-09-13 22:40:20',3),('2014-12-10 23:00:00.000','PT60M','10YSE-1--------K','CTA','SE CTA','SE','46WGU0000000046R','Stalon G1','Hydro Water Reservoir',29.2,NULL,120,'2017-09-13 22:40:20',4),('2014-12-20 07:00:00.000','PT60M','10YPT-REN------W','CTA','PT CTA','PT','16WMIRAN3------8','Miranda - G3','Hydro Run-of-river and poundage',21.4,0,60,'2017-09-13 22:40:32',5),('2014-12-20 08:00:00.000','PT60M','10YPT-REN------W','CTA','PT CTA','PT','16WMIRAN3------8','Miranda - G3','Hydro Run-of-river and poundage',50,0,60,'2017-09-13 22:40:32',6),('2014-12-11 00:00:00.000','PT60M','10YSE-1--------K','CTA','SE CTA','SE','46WGU0000000046R','Stalon G1','Hydro Water Reservoir',0,NULL,120,'2017-09-13 22:40:20',7),('2014-12-20 09:00:00.000','PT60M','10YPT-REN------W','CTA','PT CTA','PT','16WMIRAN3------8','Miranda - G3','Hydro Run-of-river and poundage',59.4,0,60,'2017-09-13 22:40:32',8),('2014-12-28 05:00:00.000','PT15M','10YHU-MAVIR----U','CTA','HU CTA','HU','15WCSEPELG1--GTH','CSP_GT1','Fossil Gas',0.1,NULL,142,'2017-09-13 22:40:43',9);
/*!40000 ALTER TABLE `act_gen_raw` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-09 21:34:37
