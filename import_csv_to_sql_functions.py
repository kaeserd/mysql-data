# -*- coding: utf-8 -*-

import os
import time
import pandas as pd

# function used to insert data into sql database

def insert_into_sql_table(engine, data_dir, mysql_schema, mysql_table,
                          filtering_kw = None):
    # drop table if already existant
    connection = engine.connect()
    connection.execute("DROP TABLE IF EXISTS "+mysql_schema+"."+mysql_table+";")
    connection.close()
    files = os.listdir(data_dir)
    for file in files:
        # checking if keyword is in filename, else file will not be imported
        if ((filtering_kw is not None and filtering_kw in file) or (filtering_kw is None)):
            print("##############################################")
            print(file)
            start_time = time.time()
            # load csv file as pandas dataframe
            df = pd.read_csv(os.path.join(data_dir, file), sep = '\t')
            intermediate_time = time.time()
            # insert dataframe into mysql database
            df.to_sql(name = mysql_table, con = engine, schema = mysql_schema, 
                      if_exists = 'append', chunksize = 10000, method = 'multi', index = False)
            end_time = time.time()
            print("csv load time: ", round(intermediate_time-start_time,0), "s")
            print("Insert time: ", round(end_time - intermediate_time,0), "s")
            print("Total Time: ", round((end_time - start_time)/60,1), "m")
    return True
