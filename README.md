# mysql data

I run also:

-show create table plant_information;

'plant_information', 'CREATE TABLE `plant_information` (\n `ResolutionCode` text,\n `AreaCode` text,\n `AreaTypeCode` text,\n `AreaName` text NOT NULL,\n `MapCode` text,\n `GenerationUnitEIC` text,\n `PowerSystemResourceName` text NOT NULL,\n `ProductionType` text NOT NULL,\n `InstalledGenCapacity` double DEFAULT NULL,\n `id_plant` int NOT NULL AUTO_INCREMENT,\n PRIMARY KEY (`id_plant`),\n KEY `country` (`MapCode`(500)),\n KEY `installed` (`InstalledGenCapacity`),\n KEY `production_type` (`ProductionType`(500)),\n KEY `time_intervall` (`ResolutionCode`(500)),\n KEY `cta` (`AreaTypeCode`(500))\n) ENGINE=InnoDB AUTO_INCREMENT=2154 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci'


-show create table act_gen_raw;
'act_gen_raw', 'CREATE TABLE `act_gen_raw` (\n `DateTime` text,\n `ResolutionCode` text,\n `AreaCode` text,\n `AreaTypeCode` text,\n `AreaName` text,\n `MapCode` text,\n `GenerationUnitEIC` text,\n `PowerSystemResourceName` text,\n `ProductionType` text,\n `ActualGenerationOutput` double DEFAULT NULL,\n `ActualConsumption` double DEFAULT NULL,\n `InstalledGenCapacity` double DEFAULT NULL,\n `UpdateTime` text,\n `id_plant` int NOT NULL AUTO_INCREMENT,\n PRIMARY KEY (`id_plant`)\n) ENGINE=InnoDB AUTO_INCREMENT=110527853 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci'

-show create table dt;
'dt', 'CREATE TABLE `dt` (\n `txt` text,\n `d` date DEFAULT NULL,\n `t` time DEFAULT NULL\n) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci'




